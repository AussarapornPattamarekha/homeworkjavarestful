package com.aussarapron.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;






@SpringBootApplication
@RestController
public class Application {

	
	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello World!";
	}
 
	//addManager
	@Autowired
	private ShopRepository shopRepository;
	
	@PostMapping(path = "/addManager", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addUser(@RequestBody Shop shop) {
		// Add Manager Detail
		Manager manager = new Manager();
		manager.setFirstname(shop.getManagers().getFirstname());
		manager.setLastname(shop.getManagers().getLastname());
		manager.setAge(shop.getManagers().getAge());
		
		shop.setManager(manager);
		shopRepository.save(shop);
		
		return "Add Manager: " + manager.getFirstname()+ " Shop: " + shop.getId();
	}
	
	//addStaff
	@Autowired
	private StaffRepository staffRepository;
	@PostMapping(path = "/addStaff", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addStaff(@RequestBody Shop shop) {
		Set<Staff>staffs = new HashSet<Staff>();
		
		Staff staff1 = new Staff();
		staff1.setFirstname("Anan");
		staff1.setLastname("Nana");
		staff1.setAge(25);
		staffs.add(staff1);
		
		shopRepository.save(shop);
		staff1.setShop(shop);
		staffRepository.save(staff1);
		shop.setStaff(staffs);
		shopRepository.save(shop);
		
				
		return "Add Staff " ;
	}
	
	
	//deleteStaff
	ArrayList<Staff> staffList = new ArrayList<Staff>();
	@DeleteMapping(path = "/delStaff")
	@ResponseBody
    public String delStaff(@RequestParam("id") int id) {
		for(Staff currentStaff :staffList ) {
			if(currentStaff.getId() == id) {
				staffList.remove(currentStaff);
				break;
			}
		}
		
		
		
		staffRepository.deleteById(id);
		
		
		return "Delete StaffOutShop " + id;
	}
	
	//editStaff
ArrayList<Staff> staffList1 = new ArrayList<Staff>();
	
	@PutMapping(path = "/editStaff/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateStaff(@PathVariable("Id") int id, @RequestBody Staff staff) {
		for (Staff currentStaff : staffList1) {
			if(currentStaff.getId() == id) {
				currentStaff.setFirstname(staff.getFirstname());
				currentStaff.setLastname(staff.getLastname());
				currentStaff.setAge(staff.getAge());
				currentStaff.setShop(staff.getShop());
				break;
			}
		}
		staffList1.add(staff);
		staffRepository.saveAll(staffList1);
		
		return "Update Staff: " + id + ","+ staff.getFirstname();
	}
	
	
	//addComputer
	@Autowired
	private ComputerRepository computerRepository;
	@PostMapping(path = "/addComputer", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String addComputer(@RequestBody Shop shop) {
		Set<Computer>computers = new HashSet<Computer>();
		
		Computer computer1 = new Computer();
		computer1.setBrand("Acer");
		computer1.setHarddisk("300");
		computer1.setCpu("3");
		computer1.setRam("3");
		
		
		shopRepository.save(shop);
		computer1.setShop(shop);
		computerRepository.save(computer1);
		shop.setSComputer(computers);
		shopRepository.save(shop);
		
				
		return "Add Computer " ;
	}
	
 //deleteComputer 
	ArrayList<Computer> comList = new ArrayList<Computer>();
	@DeleteMapping(path = "/delComputer")
	@ResponseBody
    public String delCom(@RequestParam("id") int id) {
		for(Computer currentCom :comList ) {
			if(currentCom.getId() == id) {
				comList.remove(currentCom);
				break;
			}
		}
		
		
		
		computerRepository.deleteById(id);
		
		
		return "Delete ComputerOutShop " + id;
	}
	
	//editComputer
	ArrayList<Computer> computerList = new ArrayList<Computer>();
	
	@PutMapping(path = "/editComputer/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateComputer(@PathVariable("Id") int id, @RequestBody Computer computer) {
		for (Computer currentComputer : computerList) {
			if(currentComputer.getId() == id) {
				currentComputer.setBrand(computer.getBrand());
				currentComputer.setCpu(computer.getCpu());
				currentComputer.setHarddisk(computer.getHarddisk());
				currentComputer.setRam(computer.getRam());
				break;
			}
		}
		computerList.add(computer);
		computerRepository.saveAll(computerList);
		
		return "Update Computer: " + id + ","+ computer.getBrand();
	}
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	
		
	
}
