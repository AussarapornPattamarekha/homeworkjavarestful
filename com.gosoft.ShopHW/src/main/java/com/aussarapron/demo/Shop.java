package com.aussarapron.demo;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Shop {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    //@Column(name = "id", unique = false)

	private int id;
	private String shopName;
	private String shopAddress;
	
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "manager_id", referencedColumnName = "id")
    private Manager manager;

	
	
	public Manager getManagers() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}
	
	
	
	@OneToMany(	mappedBy = "shop")
	private Set<Staff> staffs;
	
	public Set<Staff> getStaff() {
		return staffs;
	}

	public void setStaff(Set<Staff> staffs) {
		this.staffs = staffs;
	}
	
	

	@OneToMany(	mappedBy = "shop")
	private Set<Computer> computers;
	
	public Set<Computer> getComputer() {
		return computers;
	}

	public void setSComputer(Set<Computer> computers) {
		this.computers = computers;
	}
	
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopnName) {
		this.shopName = shopnName;
	}
	public String getShopAddress() {
		return shopAddress;
	}
	public void setShopAddress(String shopAddress) {
		this.shopAddress = shopAddress;
	}
	
	
	
}
