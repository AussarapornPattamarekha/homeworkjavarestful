package com.aussarapron.demo;

import org.springframework.data.repository.CrudRepository;

public interface ComputerRepository extends CrudRepository<Computer, Integer> {

}
