package com.aussarapron.demo;

import org.springframework.data.repository.CrudRepository;



public interface ShopRepository extends CrudRepository<Shop, Integer>  {

}
