package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {
	

	@Id

	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id ;
	private String name;
	private String lastname;
	private int salary;
	private String position;
	
	public Employee (int id , String name, String lastname,int salary,String position) {
		this.id = id;
		this.name = name;
		this.lastname = lastname;
		this.salary = salary;
		this.position = position;
		
		
	}
	public Employee () {
		
		
		
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}
	

}
