package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
@Entity
public class Report {
	
	@Id

	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private int idShop;
	private int idComputer;
	private int idEDC;
	private int idBarcodeScanner;
	private int numIn;
	private int numOut;
	//private int BarcodeScannerIn;
	//private int BarcodeScannerOut;
	//private int numEDCIn;
	//private int numEDCOut;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIdShop() {
		return idShop;
	}
	public void setIdShop(int idShop) {
		this.idShop = idShop;
	}
	/*
	public int getNumComputerIn() {
		return numComputerIn;
	}
	public void setNumComputerIn(int numComputerIn) {
		this.numComputerIn = numComputerIn;
	}
	public int getNumComputerOut() {
		return numComputerOut;
	}
	public void setNumComputerOut(int numComputerOut) {
		this.numComputerOut = numComputerOut;
	}
	public int getBarcodeScannerIn() {
		return BarcodeScannerIn;
	}
	public void setBarcodeScannerIn(int barcodeScannerIn) {
		BarcodeScannerIn = barcodeScannerIn;
	}
	public int getBarcodeScannerOut() {
		return BarcodeScannerOut;
	}
	public void setBarcodeScannerOut(int barcodeScannerOut) {
		BarcodeScannerOut = barcodeScannerOut;
	}
	public int getNumEDCIn() {
		return numEDCIn;
	}
	public void setNumEDCIn(int numEDCIn) {
		this.numEDCIn = numEDCIn;
	}
	public int getNumEDCOut() {
		return numEDCOut;
	}
	public void setNumEDCOut(int numEDCOut) {
		this.numEDCOut = numEDCOut;
	}
*/
	public int getIdComputer() {
		return idComputer;
	}
	public void setIdComputer(int idComputer) {
		this.idComputer = idComputer;
	}
	public int getIdEDC() {
		return idEDC;
	}
	public void setIdEDC(int idEDC) {
		this.idEDC = idEDC;
	}
	public int getIdBarcodeScanner() {
		return idBarcodeScanner;
	}
	public void setIdBarcodeScanner(int idBarcodeScanner) {
		this.idBarcodeScanner = idBarcodeScanner;
	}
	public int getNumIn() {
		return numIn;
	}
	public void setNumIn(int numIn) {
		this.numIn = numIn;
	}
	public int getNumOut() {
		return numOut;
	}
	public void setNumOut(int numOut) {
		this.numOut = numOut;
	}
}
