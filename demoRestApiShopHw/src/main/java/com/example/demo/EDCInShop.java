package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class  EDCInShop {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private int idShop;
	private int idEDC;
	
	public EDCInShop(int id,int idShop, int idEDC) {
		this.id = id;
		this.idShop = idShop;
		this.idEDC = idEDC;
	}

	
	
	
	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public int getIdShop() {
		return idShop;
	}

	public void setIdShop(int idShop) {
		this.idShop = idShop;
	}

	public int getIdEDC() {
		return idEDC;
	}

	public void setIdEDC(int idEDC) {
		this.idEDC = idEDC;
	}
	
	
	

}
