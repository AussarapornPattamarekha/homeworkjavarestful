package com.example.demo;

import java.util.ArrayList;
import java.util.Optional;

import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.aussarapron.demo.User;



@SpringBootApplication
@RestController

public class DemoRestApiShopHwApplication {

	

	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello World!!!!!!";
	}

	// addNewComputer
	@Autowired
	private ComputerRepository computerRepository;

	@PostMapping(path = "/computer/addComputer")
	public @ResponseBody String addComputer(@RequestParam String brand, @RequestParam String cpu,
			@RequestParam String ram, @RequestParam String harddisk) {

		Computer com = new Computer(null, brand, cpu, ram, harddisk);
		computerRepository.save(com);
		return "Saved";
	}

	// addEDC
	@Autowired
	private EDCRepository edcRepository;

	@PostMapping(path = "/EDC/addEDC")
	public @ResponseBody String addEDC(@RequestParam String name, @RequestParam int price) {

		EDC edcs = new EDC(0, name, price);
		edcRepository.save(edcs);
		return "Saved EDC";
	}

	// addBarcodeScanner
	@Autowired
	private BarcodeScannerRepository barcodeRepository;

	@PostMapping(path = "/BarcodeScanner/addBarcode")
	public @ResponseBody String addBarcode(@RequestParam String name, @RequestParam int price) {

		BarcodeScanner barcode = new BarcodeScanner(0, name, price);
		barcodeRepository.save(barcode);
		return "Saved BarcodeScanner";
	}

	// addEmployee
	@Autowired
	private EmployeeRepository employeeRepository;

	@PostMapping(path = "/Employee/addEmployee")
	public @ResponseBody String addEmployee(@RequestParam String name, @RequestParam String lastname,
			@RequestParam int salary, @RequestParam String position) {
		Employee employee = new Employee(0, name, lastname, salary, position);
		employeeRepository.save(employee);
		return "Saved Employee";

	}

	// addShop
	@Autowired
	private ShopRepository shopRepository;

	@PostMapping(path = "/shop/addShop")
	public @ResponseBody String addShop(@RequestParam String name, @RequestParam String address) {
		Shop shop = new Shop();
		shop.setName(name);
		shop.setAddress(address);
		shopRepository.save(shop);

		return "Saved Shop";

	}

	//GetShop
	@GetMapping(path = "/shop/showShop")
	public @ResponseBody Iterable<Shop> getAllUsers() {
		// This returns a JSON or XML with the users
		return shopRepository.findAll();

	}
	
	
	// addComputerInShop + InReport
	@Autowired
	private ComputerInShopRepository computerinshopRepository;
	
	@Autowired
	private ReportRepository reportRepository;

	@PostMapping(path = "/shop/addComInShop")
	public @ResponseBody String addComInShop(@RequestParam int idShop, @RequestParam int idComputer) {

		ComputerInShop comShop = new ComputerInShop(0, idShop, idComputer);
		comShop.setIdShop(idShop);
		comShop.setIdComputer(idComputer);
		computerinshopRepository.save(comShop);
		
		
		Report report = new Report();
		report.setIdShop(idShop);
		report.setIdComputer(idComputer);
		report.setNumIn(1);
		reportRepository.save(report);
		
		
		return "ComputerInShop";

	}
	
	//GetReport
	@GetMapping(path = "/report")
	public @ResponseBody Iterable<Report> getAllReport() {
		// This returns a JSON or XML with the users
		return reportRepository.findAll();

	}

	
	
	//EDCinShop + Report
	@Autowired
	private EDCInShopRepository EDCinshopRepository;
	
	@PostMapping(path = "/shop/addEDCInShop")
	public @ResponseBody String addEDCInShop(@RequestParam int idShop, @RequestParam int idEDC) {

		EDCInShop edcsShop = new EDCInShop(0, idShop, idEDC);
		edcsShop.setIdShop(idShop);
		edcsShop.setIdEDC(idEDC);
		EDCinshopRepository.save(edcsShop);
		
		Report report = new Report();
		report.setIdShop(idShop);
		report.setIdEDC(idEDC);
		report.setNumIn(1);
		reportRepository.save(report);
		
		
		return "EDCInShop";

	}
	
	//BarcodeScanneerinShop + Report
	@Autowired
	private BarcodeInShopRepository barcodeinshopRepository;
	
	@PostMapping(path = "/shop/addBarcodeInShop")
	public @ResponseBody String addBarcodeInShop(@RequestParam int idShop, @RequestParam int idBarcode) {

		BarcodeInShop barcodeShop = new BarcodeInShop(0, idShop, idBarcode);
		barcodeShop.setIdShop(idShop);
		barcodeShop.setIdBarcode(idBarcode);
		barcodeinshopRepository.save(barcodeShop);
		
		Report report = new Report();
		report.setIdShop(idShop);
		report.setIdBarcodeScanner(idBarcode);
		report.setNumIn(1);
		reportRepository.save(report);
		
		
		return "BarcodeScannerInShop";

	}
	
	
	
	
	ArrayList<ComputerInShop> comList = new ArrayList<ComputerInShop>();
	

	//DeleteComOutShop + Report
	@DeleteMapping(path = "shop/deleteComOutShop")
	@ResponseBody
    public String deleteComOutShop(@RequestParam("id") int id,@RequestParam("idShop") int idshop,@RequestParam("idComputer") int idcomputer) {
		for(ComputerInShop currentCom :comList ) {
			if(currentCom.getId() == id) {
				comList.remove(currentCom);
				break;
			}
		}
		
		Report report = new Report();
		report.setIdShop(idshop);
		report.setIdComputer(idcomputer);
		report.setNumOut(1);
		reportRepository.save(report);
		
		computerinshopRepository.deleteById(id);
		
		
		return "Delete ComputerOutShop " + id;
	}
	
	
	ArrayList<Computer> computerList = new ArrayList<Computer>();
	//editComputer
	@PutMapping(path = "/editComputer/{Id}", consumes = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	String updateComputer(@PathVariable("Id") int id, @RequestBody Computer computer) {
		for (Computer currentComputer : computerList) {
			if(currentComputer.getId() == id) {
				currentComputer.setBrand(computer.getBrand());
				currentComputer.setCpu(computer.getCpu());
				currentComputer.setHarddisk(computer.getHarddisk());
				currentComputer.setRam(computer.getRam());
				break;
			}
		}
		computerList.add(computer);
		computerRepository.saveAll(computerList);
		
		return "Update Computer: " + id + ","+ computer.getBrand();
	}
	
	
	
	public static void main(String[] args) {
		SpringApplication.run(DemoRestApiShopHwApplication.class, args);
	}

}
