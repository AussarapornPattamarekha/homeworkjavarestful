package com.example.demo;

import java.util.ArrayList;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.mapping.List;
import org.springframework.beans.factory.annotation.Autowired;

@Entity

public class Shop {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
    //@Column(name = "id", unique = false)

	private int id;
	private String name;
	private String address;
	
	
	//private ArrayList<Employee> employees = new ArrayList<Employee>();
	
	
	//private ArrayList<Computer> computers = new ArrayList<Computer>();
	
	
	//private ArrayList<EDC> edcs = new ArrayList<EDC>();
	
	
	//private ArrayList<BarcodeScanner> barcodes = new ArrayList<BarcodeScanner>();
	// private Report totalReport = new Report();

	
	
	
	/*
	public void addComputer(Computer computer) {
		computers.add(computer);
		// totalReport.setNumComputerIn(totalReport.getNumComputerIn() + 1);
	}
	*/

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	/*
	public ArrayList<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(ArrayList<Employee> employees) {
		this.employees = employees;
	}

	public ArrayList<EDC> getEdcs() {
		return edcs;
	}

	public void setEdcs(ArrayList<EDC> edcs) {
		this.edcs = edcs;
	}
	*/
	
/*
	public Report getTotalReport() {
		return totalReport;
	}

	public void setTotalReport(Report totalReport) {
		this.totalReport = totalReport;
	}
*/
	/*
	public ArrayList<Computer> getComputers() {
		return computers;
	}

	public void setComputers(ArrayList<Computer> computers) {
		this.computers = computers;
	}
	
	public ArrayList<BarcodeScanner> getBarcodes() {
		return barcodes;
	}

	public void setBarcodes(ArrayList<BarcodeScanner> barcodes) {
		this.barcodes = barcodes;
	}
	*/

}
