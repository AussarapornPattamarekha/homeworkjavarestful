package com.example.demo;

import org.hibernate.mapping.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface ComputerInShopRepository extends CrudRepository<ComputerInShop, Integer> {
	
}
