package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class  BarcodeInShop {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private int idShop;
	private int idBarcode;
	
	public BarcodeInShop(int id,int idShop, int idBarcode) {
		this.id = id;
		this.idShop = idShop;
		this.idBarcode = idBarcode;
	}

	
	
	
	public int getId() {
		return id;
	}




	public void setId(int id) {
		this.id = id;
	}




	public int getIdShop() {
		return idShop;
	}

	public void setIdShop(int idShop) {
		this.idShop = idShop;
	}




	public int getIdBarcode() {
		return idBarcode;
	}




	public void setIdBarcode(int idBarcode) {
		this.idBarcode = idBarcode;
	}

	
	
	

}
