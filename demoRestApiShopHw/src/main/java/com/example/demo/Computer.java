package com.example.demo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Computer {

	@Id

	@GeneratedValue(strategy=GenerationType.AUTO)

	
	private Integer id;
	private String brand;
	private String cpu;
	private String ram;
	private String harddisk;
	
	public Computer(Integer id,String brand,String cpu,String ram,String harddisk) {
		
		this.id = id;
		this.brand = brand;
		this.cpu = cpu;
		this.ram = ram;
		this.harddisk = harddisk;
		
	}

	public Computer() {

	}

	public Computer(Integer id) {
		this.id = id;
	}


	public Integer getId() {
		return id;
	}



	public void setId(Integer id) {
		this.id = id;
	}



	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getHarddisk() {
		return harddisk;
	}

	public void setHarddisk(String harddisk) {
		this.harddisk = harddisk;
	}

}
